package com.muhardin.endy.training.microservice.bankingapi.dto;

import lombok.Data;

@Data
public class Nasabah {
    private Long id;

    private String nama;

    private String email;

    private String noHp;
}