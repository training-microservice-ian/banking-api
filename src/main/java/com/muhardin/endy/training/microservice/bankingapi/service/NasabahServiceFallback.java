package com.muhardin.endy.training.microservice.bankingapi.service;

import java.util.ArrayList;

import com.muhardin.endy.training.microservice.bankingapi.dto.Nasabah;

import org.springframework.stereotype.Component;

@Component
public class NasabahServiceFallback implements NasabahService {

    @Override
    public Iterable<Nasabah> ambilDataNasabah() {
        System.out.println("Backend service offline, menjalankan fallback");
        return new ArrayList<Nasabah>();
    }
}