package com.muhardin.endy.training.microservice.bankingapi.controller;

import com.muhardin.endy.training.microservice.bankingapi.dto.Nasabah;
import com.muhardin.endy.training.microservice.bankingapi.service.NasabahService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NasabahController {

    @Autowired private NasabahService nasabahService;

    @GetMapping("/nasabah/")
    public Iterable<Nasabah> dataNasabah(){
        return nasabahService.ambilDataNasabah();
    }
}